#ifndef AttackCircle_h
#define AttackCircle_h

#include "Arduino.h"
#include <ArduinoSTL.h>
#include <vector>
using namespace std;

class AttackCircle
{
public:
	AttackCircle();
  int run(byte weapon_id);
  void start();
  byte stop();
  void setLEDOn(byte led);
  void setLEDsOff();
  bool blinkLED(byte led);
  int blinkDelay();
  bool isRunning();

private:
	void setRunningMovement(byte weapon_id);
	byte step();

	const byte numPins = 4;
	vector<byte> pins;
  byte deactivate_pin;

	vector<byte> sequence;
	byte last_weapon_used;

	int interval;
	byte current_pos;

  bool stop_running;

  int blink_counter;
  int blink_times;
  int blink_duration;
};

#endif
