#include "AttackCircle.h"
#include "ApplicationState.h"
#include "Button.h"
#include "Battle.h"

//PINOUT:
//  13 -> SD Card SCK
//  12 -> SD Card Miso
//  11 -> SD Card Mosi
//  10 -> Attack Circle MSB
//  9 ->  Attack Circle bit
//  8 ->  Attack Circle LSB
//  7 ->  Select Button
//  6 ->  Right Button
//  5 ->  Left Button
//  4 ->  SD Card CS
//  3 ->  Free
//  2 ->  Free
//  1 ->  TX
//  0 ->  RX

// A5 ->  SCL -> OLED SCL
// A4 ->  SDA -> OLED SDA
// A3 ->  Free
// A2 ->  Free
// A1 ->  Free
// A0 ->  Free

AttackCircle attack_circle;
ApplicationState app;

Button select_button(7);
Button right_button(6);
Button left_button(5);
//Button up_button(4);
//Button down_button(3);

byte attack_result;
bool blink_end;

int run_delay;

byte battle_progress_counter;
bool finished_prologue;
bool finished_epilogue;

Battle battle1(1, 4, 2);

//TODO: implement these methods
bool isBookClosed() {
  return true;
}

void suspend() {
  return;
}

byte recover() {
  //returns state before open book and recover all variables values
  return READING_TAG;
}

byte readTag() {
  return 1;
}

void setup() {
  attack_result = 255;
//  attack_circle.start();
  Serial.begin(9600);

  //for tests only
  app.toGODMode();
  battle_progress_counter = 0;
  finished_prologue = false;
  finished_epilogue = false;
}

void loop() {
  switch(app.getState()) {
    case BEGIN:
      if(isBookClosed()) {
        app.toClosed();
      } else {
        app.toOpened();
      }
    break;
    case GOD:
      if(Serial.available() > 0) {
        app.injectState(Serial.parseInt());
      }
    break;
    
//    case OPENED:
//      suspend();
//      if(isBookClosed()) {
//        app.toClosed();
//      }
//    break;
//    case CLOSED:
//      if(app.getPrevState() == BEGIN) {
//        app.toReadingTag();
//      }   
//      //TODO: Implement recovering  
//    break;
//    case READING_TAG:
//      current_mission = readTag();
//      app.toMissionMenu();
//    break;

      case ATTACK_CIRCLE:
        if(!attack_circle.isRunning()) {
          attack_circle.start();
        }
        attack_circle.setLEDsOff();
        delay(50);
        run_delay = attack_circle.run(0);
        delay(run_delay);
        
        if(select_button.clicked()) {
//          Serial.println("Clicked");
          attack_result = attack_circle.stop();
        }
      
        if(attack_result < 255) {
//          Serial.println("Blink");
          if(attack_circle.blinkLED(attack_result)) {
            delay(attack_circle.blinkDelay());
          } else { 
            delay(500);
            attack_circle.setLEDsOff();

            //temporary
            if(!app.isGOD) {
              app.toPerformAction();
            } else {
              app.toGODMode();
            }
          }
        }
      break;

      case BATTLE:
        if(!finished_prologue) {
          battle_progress_counter = battle1.prologue(battle_progress_counter);
        } else if(!finished_epilogue){
          battle_progress_counter = battle1.epilogue(battle_progress_counter);
        }

        if(right_button.clicked()) {
          battle_progress_counter = battle_progress_counter + 1;
        } else if (left_button.clicked()) {
          battle_progress_counter = battle_progress_counter - 1;
        } else if (select_button.clicked()) {
          if(!finished_prologue) {
            finished_prologue = true;
          } else if(!finished_epilogue){
            finished_epilogue = true;

            if(!app.isGOD) {
              app.toMissionMenu();
            } else {
              app.toGODMode();
            }
          }
        }

        delay(50);
      break;
  }
}
