#include "Button.h"
#include "Arduino.h"

//===================== PRIVATE ==============================================


//======================= PUBLIC =============================================

Button::Button(byte buttonPin) {
	pinMode(buttonPin, INPUT);
  	this->buttonPin = buttonPin;

  	this->buttonState = LOW;
	this->pressed = false;
}

bool Button::clicked() {
	buttonState = digitalRead(this->buttonPin);

	if (buttonState == LOW) {
		if(!pressed) {
			pressed = true;
			return true;
		}
	} else {
		pressed = false;
	}

	return false;
}
