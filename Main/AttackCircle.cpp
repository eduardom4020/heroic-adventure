#include "AttackCircle.h"
#include "Arduino.h"
#include <vector>
using namespace std;

//===================== PRIVATE ==============================================

void AttackCircle::setRunningMovement(byte weapon_id) {
	if(this->last_weapon_used != weapon_id) {
		switch(weapon_id) {
			case 1:
				this->sequence = {0, 8, 1, 9, 2, 10, 3, 11, 4, 12, 5, 13, 6, 14, 7, 15};
				this->interval = 100;
			break;
			default:
				this->sequence = {0, 1, 2, 3, 4, 5, 6, 7};
				this->interval = 200;
			break;
		}
	}
}

byte AttackCircle::step() {
	return this->current_pos == this->sequence.size() - 1 ? 0 : this->current_pos + 1;
}

//======================= PUBLIC =============================================

AttackCircle::AttackCircle() {
	this->pins = {10, 9, 8};
//  this->deactivate_pin = 9;
  
	this->last_weapon_used = -1;
	this->current_pos = 0;

  for(byte &pin : this->pins) {
    pinMode(pin, OUTPUT);
  }
//  pinMode(this->deactivate_pin, OUTPUT);
  
//  digitalWrite(this->deactivate_pin, HIGH);

  this->stop_running = true;

  this->blink_counter = 0;
  this->blink_times = 5;
  this->blink_duration = 5000;
}
//todo: correct this. remove delays change for multiples returns. make delay only on main
bool AttackCircle::blinkLED(byte led) {
  if(this->blink_counter < this->blink_times * 2) {
    if(this->blink_counter % 2 == 0) {
      this->setLEDOn(led);
    } else {
      this->setLEDsOff();
    }
    
    this->blink_counter = this->blink_counter + 1;
    return true;
  } else {
    this->blink_counter = 0;
    return false;
  }
}

int AttackCircle::blinkDelay() {
  return this->blink_duration / this->blink_times / 2;
}

void AttackCircle::setLEDOn(byte led) {
//  digitalWrite(this->deactivate_pin, LOW);
  
  for (byte i=0; i<this->numPins; i++) {
    byte state = bitRead(led, i);
    digitalWrite(this->pins[i], state);
  }
}

void AttackCircle::setLEDsOff() {
//  digitalWrite(this->deactivate_pin, HIGH);
  
  for (byte i=0; i<this->numPins; i++) {
    digitalWrite(this->pins[i], HIGH);
  }
}

int AttackCircle::run(byte weapon_id) {
  if(!this->stop_running) {
  	this->setRunningMovement(weapon_id);	
    this->setLEDOn(this->sequence[this->current_pos]);
    this->current_pos = this->step();

  	return(this->interval);
  }
}

void AttackCircle::start() {
  this->stop_running = false;
  this->current_pos = 0;
}

byte AttackCircle::stop() {
  this->stop_running = true;
	return this->sequence[this->current_pos];
}

bool AttackCircle::isRunning() {
  return !this->stop_running;
}
