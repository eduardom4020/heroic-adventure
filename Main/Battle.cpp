#include "Battle.h"
#include "TSVManager.h"
#include "U8glib.h"
#include "Arduino.h"

//===================== PRIVATE ==============================================


//======================= PUBLIC =============================================
Battle::Battle(byte id, byte prologue_steps, byte epilogue_steps) {
  this->id = id;
  this->prologue_steps = prologue_steps;
  this->epilogue_steps = epilogue_steps;
  
  this->tsv = new TSVManager("", 0 ,0);

  this->u8g = new U8GLIB_SSD1306_128X64(U8G_I2C_OPT_NO_ACK);
}

void Battle::showText(byte progress) {
  this->u8g->firstPage();
  do {
    this->u8g->setFont(u8g_font_8x13B);
    this->u8g->drawStr(5, 5, this->tsv->access(1, progress+1).c_str());
  } while (this->u8g->nextPage());
}

byte Battle::prologue(byte progress) {
  if(progress + 1 == this->prologue_steps) {
    progress = this->prologue_steps - 1;
  }

  if(progress < 0) {
    progress = 0;
  }
   
  String title = "Battle_" + String(id);
  title = title + "_Prologue.tsv";

  this->tsv->setFile(title);
  this->showText(progress);

  return progress;
}

byte Battle::epilogue(byte progress) {
  if(progress + 1 == this->epilogue_steps) {
    progress = this->epilogue_steps - 1;
  }

  if(progress < 0) {
    progress = 0;
  }
  
  String title = "Battle_" + String(id);
  title = title + "_Epilogue.tsv";

  this->tsv->setFile(title);
  this->showText(progress);

  return progress;
}
