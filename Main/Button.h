#ifndef Button_h
#define Button_h

#include "Arduino.h"

class Button
{
private:
  byte buttonPin;
  byte buttonState;

public:
  Button(byte buttonPin);
  bool clicked();
  bool pressed;
};

#endif
