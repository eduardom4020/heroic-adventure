#ifndef TSVManager_h
#define TSVManager_h

#include "Arduino.h"
#include <SPI.h>
#include <SD.h>

class TSVManager
{
private:
  byte header_cursor;
  byte row_cursor;
  String file;

public:
  TSVManager(String file, byte header_cursor, byte row_cursor);
  void setFile(String file);
  String access(byte header_cursor, byte row_cursor);
  String next();
};

#endif
