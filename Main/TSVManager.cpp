#include "TSVManager.h"
#include "Arduino.h"

//===================== PRIVATE ==============================================


//======================= PUBLIC =============================================

TSVManager::TSVManager(String file, byte header_cursor, byte row_cursor) {
  this->header_cursor = header_cursor;
  this->row_cursor = row_cursor;
  this->file = file;

  if (!SD.begin(4)) {
    Serial.println("sd card initialization failed!");
  }
}

void TSVManager::setFile(String file) {
  this->file = file;
}

String TSVManager::access(byte header_cursor, byte row_cursor) {
  File file = SD.open(this->file);
  String result = "";

  this->header_cursor = header_cursor;
  this->row_cursor = row_cursor;
  
  while (file.available()) {
      char c = file.read();
      if (c == '\t') {
          this->header_cursor = this->header_cursor + 1;
          return result;
      } else if (c == '\n') {
          this->header_cursor = 0;
          this->row_cursor = this->row_cursor + 1;
          return "EOL";
      } else {
          result = result + c;
      }
  }
}

String TSVManager::next() {
  return this->access(this->header_cursor + 1, this->row_cursor);
}
