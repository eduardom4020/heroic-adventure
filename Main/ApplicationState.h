#ifndef ApplicationState_h
#define ApplicationState_h

#include "Arduino.h"

#define GOD             -1
#define BEGIN           0
#define OPENED          1
#define CLOSED          2
#define READING_TAG     3
#define MISSION_MENU    4
#define BATTLE          5
#define BATTLE_TURN     6
#define ACTION_MENU     7
#define ENEMY_ACTION    8
#define ATTACK_MENU     9
#define ATTACK_CIRCLE   10
#define PERFORM_ACTION  11

class ApplicationState
{ 
private:
  byte current;
  byte previous;

  bool canStateChangeTo(byte state);
  bool changeTo(byte state);
public:
  ApplicationState();
  byte getState();
  byte getPrevState();
  bool toOpened();
  bool toClosed();
  bool toReadingTag();
  bool toMissionMenu();
  bool toBattle();
  bool toBattleTurn();
  bool toActionMenu();
  bool toEnemyAction();
  bool toAttackMenu();
  bool toAttackCircle();
  bool toPerformAction();

  bool toGODMode();
  void injectState(byte state);

  byte isGOD;
};

#endif
