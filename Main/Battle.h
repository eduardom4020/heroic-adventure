#ifndef Battle_h
#define Battle_h

#include "Arduino.h"
#include "TSVManager.h"
#include "U8glib.h"

class Battle
{
private:
  byte id;
  byte prologue_steps;
  byte epilogue_steps;
  
  TSVManager* tsv;
  U8GLIB_SSD1306_128X64* u8g;

  void showText(byte progress);
  
public:
  Battle(byte id, byte prologue_steps, byte epilogue_steps);
  byte prologue(byte progress);
  void battling();
  byte epilogue(byte progress);
};

#endif
