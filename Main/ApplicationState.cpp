#include "ApplicationState.h"
#include "Arduino.h"

//===================== PRIVATE ==============================================
bool ApplicationState::canStateChangeTo(byte state) {
  switch(this->current) {
    case GOD:
      return true;
      
    case BEGIN:
      return (
        state == OPENED || 
        state == CLOSED
      );

    case OPENED:
      return (
        state == CLOSED
      );

    case CLOSED:
      return (
        state == OPENED || 
        state == READING_TAG
      );
    break;
    case READING_TAG:
      return (
        state == OPENED || 
        state == MISSION_MENU
      );

    case MISSION_MENU:
      return (
        state == OPENED || 
        state == BATTLE
      );

    case BATTLE:
      return (
        state == OPENED || 
        state == BATTLE_TURN
      );
    break;
    case BATTLE_TURN:
      return (
        state == OPENED || 
        state == ACTION_MENU ||
        state == ENEMY_ACTION
      );

    case ENEMY_ACTION:
      return (
        state == OPENED || 
        state == PERFORM_ACTION
      );

    case ACTION_MENU:
      return (
        state == OPENED || 
        state == ATTACK_MENU
      );
    break;
    case ATTACK_MENU:
      return (
        state == OPENED || 
        state == ATTACK_CIRCLE
      );

    case PERFORM_ACTION:
      return (
        state == OPENED || 
        state == BATTLE
      );
  }
}

bool ApplicationState::changeTo(byte state) {
  if(this->isGOD || this->canStateChangeTo(state)) {
    this->previous = this->current;
    this->current = state;
    return true;
  } else {
    return false;
  }
}

//======================= PUBLIC =============================================
ApplicationState::ApplicationState() {
  this->current = BEGIN;
  this->previous = BEGIN;
  this->isGOD = false;
}

bool ApplicationState::toGODMode() {
  this->isGOD = true;
  return this->changeTo(GOD);
}

byte ApplicationState::getState() {
  return this->current;
}

byte ApplicationState::getPrevState() {
  return this->previous;
}

void ApplicationState::injectState(byte state) {
  this->changeTo(state);
}

bool ApplicationState::toOpened() {
  return this->changeTo(OPENED);
}

bool ApplicationState::toClosed() {
  return this->changeTo(CLOSED);
}

bool ApplicationState::toReadingTag() {
  return this->changeTo(READING_TAG);
}

bool ApplicationState::toMissionMenu() {
  return this->changeTo(MISSION_MENU);
}

bool ApplicationState::toBattle() {
  return this->changeTo(BATTLE);
}

bool ApplicationState::toBattleTurn() {
  return this->changeTo(BATTLE_TURN);
}

bool ApplicationState::toActionMenu() {
  return this->changeTo(ACTION_MENU);
}

bool ApplicationState::toEnemyAction() {
  return this->changeTo(ENEMY_ACTION);
}

bool ApplicationState::toAttackMenu() {
  return this->changeTo(ATTACK_MENU);
}

bool ApplicationState::toAttackCircle() {
  return this->changeTo(ATTACK_CIRCLE);
}

bool ApplicationState::toPerformAction() {
  return this->changeTo(PERFORM_ACTION);
}
