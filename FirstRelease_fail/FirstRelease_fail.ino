#include <avr/pgmspace.h>
//#include <SPI.h>
//#include <SD.h>
#include <U8g2lib.h>
#include <ArduinoSTL.h>
#include <vector>
using namespace std;

//PINOUT:
//  13 -> SD Card SCK
//  12 -> SD Card Miso
//  11 -> SD Card Mosi
//  10 -> Attack Circle MSB
//  9 ->  Attack Circle bit
//  8 ->  Attack Circle bit
//  7 ->  Select Button
//  6 ->  Right Button
//  5 ->  Left Button
//  4 ->  SD Card CS
//  3 ->  Attack Circle LSB
//  2 ->  Control Circle
//  1 ->  TX
//  0 ->  RX

// A5 ->  SCL -> OLED SCL
// A4 ->  SDA -> OLED SDA
// A3 ->  Free
// A2 ->  Free
// A1 ->  Free
// A0 ->  Free

// -----------------------------  Constants -------------------------------------
byte it;
byte aux_it;

byte var_aux;

String oled_string;
byte font_size;

// 0: select - pin7 ; 1: right - pin6 ; 2: left - pin5
const byte buttons_amt = 3;
const PROGMEM byte buttons[] = {7, 6, 5};

const byte attack_circle_pins_amt = 4;
const PROGMEM byte attack_circle_pins[] = {10, 9, 8, 3};

byte current_state = 254;
byte previous_state = current_state;
byte aux_state;

byte menu_current;
//byte menu_previous;

const char main_menu_1[] PROGMEM = "Heróis ao Resgate!";
const char menu_empty[] PROGMEM = "--";

const char* const main_menu[] PROGMEM = {main_menu_1, menu_empty, menu_empty, menu_empty};

const char action_menu_1[] PROGMEM = "Atacar";
const char action_menu_2[] PROGMEM = "Analizar";
const char action_menu_3[] PROGMEM = "Voltar";

const char* const action_menu[] PROGMEM = {action_menu_1, action_menu_2, action_menu_3};

char str_buffer[120];

bool bt_sl_clicked;
bool bt_lt_clicked;
bool bt_rt_clicked;

byte pointer_counter;

const char misson_1_prologue_1[] PROGMEM = "Pag 1";
const char misson_1_prologue_2[] PROGMEM = "Pag 2";
const char misson_1_prologue_3[] PROGMEM = "Pag 3";
const char misson_1_prologue_4[] PROGMEM = "Pag 4";

const char* const misson_1_prologue[] PROGMEM = {misson_1_prologue_1, misson_1_prologue_2, misson_1_prologue_3, misson_1_prologue_4};

const char misson_1_epilogue_1[] PROGMEM = "Pag 1";
const char misson_1_epilogue_2[] PROGMEM = "Pag 2";
const char misson_1_epilogue_3[] PROGMEM = "Pag 3";
const char misson_1_epilogue_4[] PROGMEM = "Pag 4";

const char* const misson_1_epilogue[] PROGMEM = {misson_1_epilogue_1, misson_1_epilogue_2, misson_1_epilogue_3, misson_1_epilogue_4};

const char post_attack_pharse_1[] PROGMEM = "O inimigo sente a dor do dano levado, porém ainda está de pé.";
const char post_attack_pharse_2[] PROGMEM = "Após sofrer o dano o inimigo tenta melhorar sua guarda.";

// ---------------------------- weapons -------------------------------------------------------------
// 0: weak, 1: medium, 2: strong, 3: critical
vector<byte> strenghts = {0, 1, 0, 2, 1, 0, 0, 2, 0, 1, 1, 0, 2, 1, 1, 3};

vector<byte> knife_1_sequence = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
const byte knife_1_delay = 190;
const byte knife_1_base_dmg = 1;
const byte knife_1_range = 8;
vector<byte> bow_1_sequence = {0, 2, 4, 6, 8, 10, 12, 14, 1, 3, 5, 7, 9, 11, 13, 15};
const byte bow_1_delay = 150;
const byte bow_1_base_dmg = 2;
const byte bow_1_range = 70;
const byte bow_1_short_range = 20;
vector<byte> aux_sequence;
byte aux_delay;

//---------------------- characters base ------------------------------------------------------------
const char villager_attack_pharse_1[] PROGMEM = "Com todo o poder de um aldeão ordinário, Aldeão ";
const char villager_attack_pharse_2[] PROGMEM = "Aldeão, fazendo bom uso de sua presença indiferente, ";
const char villager_attack_pharse_3[] PROGMEM = "Oculto como um rato de esgoto, subtamente Aldeão surge e ";
const char villager_attack_pharse_4[] PROGMEM = "À grande distância, onde ninguém pudesse alcançá-lo, Aldeão ";

const char* const villager_attack_pharses[] PROGMEM = {villager_attack_pharse_1, villager_attack_pharse_2, villager_attack_pharse_3};
const char* const villager_range_attack_pharses[] PROGMEM = {villager_attack_pharse_4, villager_attack_pharse_4, villager_attack_pharse_3};

const char villager_post_attack_pharse_1[] PROGMEM = "Imediatamente após o ataque ele corre, rapidamente se distanciando com medo do contra ataque.";

//const char villager_post_defeat_pharse_1_1[] PROGMEM = "Aldeão: Mais um inimigo derrotado! Quando voltar ao reino aceitarei ser tratado como um herói! HAHAHA!!";
//const char villager_post_defeat_pharse_1_2[] PROGMEM = "Herói: Moleque, se tu gostas de ter vossa cabeça ligada ao pescoço, sugiro que não tentais roubar o que é meu...";
//const char villager_post_defeat_pharse_1_3[] PROGMEM = "Aldeão: I... isso f-foi brincadeira né? Sabe, eu na verdade me sinto muito feliz sendo apenas um plebeu...";

const char* const villager_post_attack_pharses[] PROGMEM = {villager_post_attack_pharse_1, post_attack_pharse_1, post_attack_pharse_1, post_attack_pharse_2};

const char villager_weapon_1[] PROGMEM = "Faca de Cobre";
const char villager_weapon_2[] PROGMEM = "Arco Curto";

const char* const villager_weapons[] PROGMEM = {villager_weapon_1, villager_weapon_2};

const char villager_weapon_1_ready[] PROGMEM = "Aldeão aperta com força o punho de sua faca, preparando-se para o ataque.";
const char villager_weapon_2_ready[] PROGMEM = "O arco e a flecha já estão em mãos, Aldeão procura a oportunidade perfeita para atacar.";

const char* const villager_weapons_ready[] PROGMEM = {villager_weapon_1_ready, villager_weapon_2_ready};

const char villager_weapon_1_damage_w[] PROGMEM = "corta o oponente.";
const char villager_weapon_1_damage_m[] PROGMEM = "talha o oponente.";
const char villager_weapon_1_damage_s[] PROGMEM = "crava a faca no oponente.";
const char villager_weapon_1_damage_c[] PROGMEM = "dilacera o oponente.";

const char villager_weapon_2_damage_w[] PROGMEM = "acerta o oponente.";
const char villager_weapon_2_damage_m[] PROGMEM = "perfura o oponente.";
const char villager_weapon_2_damage_s[] PROGMEM = "finca uma flecha no oponente.";
const char villager_weapon_2_damage_c[] PROGMEM = "abre um buraco no oponente.";

const char* const villager_weapon_1_damage[] PROGMEM = {villager_weapon_1_damage_w, villager_weapon_1_damage_m, villager_weapon_1_damage_s, villager_weapon_1_damage_c};
const char* const villager_weapon_2_damage[] PROGMEM = {villager_weapon_2_damage_w, villager_weapon_2_damage_m, villager_weapon_2_damage_s, villager_weapon_2_damage_c};

const byte villager_base_hearts = 4;
byte villager_hearts = villager_base_hearts;
byte villager_debuff = false;
byte villager_x;
byte villager_y;

byte pages_counter;
//bool finished_prologue;
//bool finished_epilogue;
//bool finished_battle;
bool nav_begin;

int turn;
byte turn_it;

bool battling;
bool selecting_action;
bool selecting_weapon;
bool analizing_target;
bool attack_circle;
bool review_action;
bool not_attack;

//characters index
// 0: Villager, 1: Hero, 20..29: Enemies_1

vector<byte> players;
vector<byte> enemies;
vector<byte> turn_order;

byte circle_it;

vector<byte> enemies_base_hearts;
vector<byte> enemies_hearts;
vector<byte> enemies_base_dmg;
vector<byte> enemies_range;
vector<byte> enemies_x;
vector<byte> enemies_y;

byte distance_to_enemy;
byte closest_enemy;

byte aproach;
// ----------------------------- UTILS ------------------------------------------
void clearOledString() {
  oled_string = ""; 
}

void showStateInfo() {
  Serial.print(F("Previous State: ")); Serial.print(previous_state);
  Serial.print(F(" Current State: ")); Serial.println(current_state);
}

// ------------------------------- BUTTON ---------------------------------------
// 0: select - pin7 ; 1: right - pin6 ; 2: left - pin5
void setButton(byte buttonPin) {
  pinMode(buttonPin, INPUT);
}

void buttonsSetup() {
  for(it=0; it<buttons_amt; it++) {
    setButton(pgm_read_word_near(buttons + it));
  }
}

/*Method Usage: 
    bt_x_clicked = false;
    bt_x_clicked = buttonClicked(x, bt_x_pressed)
    if(bt_x_clicked) {...}
*/
//bool buttonClicked(byte buttonPin, bool pressed) {
//  if (digitalRead(buttonPin) == HIGH) {
//    if(!pressed) {
//      return true;
//    } else {
//      return false;
//    }
//  } else {
//    return false;
//  }
//}

bool buttonClicked(byte buttonPin, bool pressed) {
  return(digitalRead(buttonPin) == HIGH);
}

//-------------------------------------------------------------------------------

// ------------------------------- STATES --------------------------------------
// States index

// 0: god, 1: begin, 2: opened, 3: closed, 4: reading_tag, 5: mission_menu
//6: battle, 7: mission_end, 253: SD READ TEST,254: initialization_check

bool canStateChangeTo(byte current, byte state) {
  switch(current) {
    case 0: return true;      
    case 1: return ( state == 3 || state == 2 || state == 0 );
    case 2: return ( state == 1 || state == 0 );
    case 3: return ( state == 5 || state == 2 || state == 0 );
    //case 4: return ( state == 5 || state == 2 || state == 0 );
    case 5: return ( state == 6 || state == 2 || state == 0 );
    case 6: return ( state == 7 || state == 2 || state == 0 );
    case 7: return ( state == 3 || state == 2 || state == 0 || state == 8 || state == 1);
    case 8: return ( state == 3 || state == 2 || state == 0 || state == 1);

    case 254: return ( state == 1 || state == 0 );
  }
}

/*Method Usage: 
    result = tansition(current_state, next_state);
    if(result < 255 and result != current_state) {
      previous_state = current_state;
      current_state = tansition(current_state, next_state);
    }
    switch(current_state) {...}
*/
byte tansition(byte current, byte state) {
  if(canStateChangeTo(current, state)) {
    return state;
  } else {
    return 255;
  }
}

//-------------------------------------------------------------------------------

// ---------------------------------- LDR ---------------------------------------
bool isClosed() {
  return true;
//  return analogRead(0) < 20;
}

//-------------------------------------------------------------------------------

void showPrologue() {
  if(nav_begin) {
    clearOledString();
    pages_counter = 0;
    setFontsize(10);
    oled_string = "Prólogo";
    printStrInOled(34, 16);
    delay(2000);
    nav_begin = false;
  }
  
  clearOledString();
  setFontsize(10);
  oled_string = strcpy_P(str_buffer, (char*)pgm_read_word(&(misson_1_prologue[pages_counter])));
  printStrInOled(4, 4);
}

void showEpilogue() {
  if(nav_begin) {
    clearOledString();
    pages_counter = 0;
    setFontsize(10);
    oled_string = "Epílogo";
    printStrInOled(32, 16);
    delay(2000);
    nav_begin = false;
  }
  
  clearOledString();
  setFontsize(10);
  oled_string = strcpy_P(str_buffer, (char*)pgm_read_word(&(misson_1_epilogue[pages_counter])));
  printStrInOled(4, 4);
}

void getMinimunDistance() {
  distance_to_enemy = abs(enemies_x[0] - villager_x) + abs(enemies_y[0] - villager_y);
  closest_enemy = 0;
  if(abs(enemies_x[1] - villager_x) + abs(enemies_y[1] - villager_y) > distance_to_enemy) {
    distance_to_enemy = abs(enemies_x[1] - villager_x) + abs(enemies_y[1] - villager_y);
    closest_enemy = 1;
  }
}

//-------------------------------------------------------------------------------

// --------------------------------- OLED ---------------------------------------
U8G2_SSD1306_128X64_NONAME_1_SW_I2C u8g2(U8G2_R0, /* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);

void drawOledStr(byte x, byte y) {
  aux_it = 0;
  var_aux = y;
  it=0;
  while(it < oled_string.length()) {
    if(it - aux_it == 140/font_size or (it - aux_it > 100/font_size and oled_string.charAt(it) == ' ')) {
      u8g2.drawUTF8(x, y, oled_string.substring(aux_it, it+1).c_str());
      y = y + font_size + 2;
      aux_it = it + 1;
      if(oled_string.charAt(aux_it) == ' ') {
        aux_it = aux_it + 1;
        it = it + 1;
      }
    }
    it = it + 1;
  }

  if(aux_it < it) {
    u8g2.drawUTF8(x, y, oled_string.substring(aux_it, it+1).c_str());
  }

  y = var_aux;
}

void printStrInOled(byte x, byte y) {
  u8g2.firstPage();
  do {
    drawOledStr(x, y);
  } while (u8g2.nextPage());
}

void setFontsize(byte val) {
  switch(val) {
    case 8:
      u8g2.setFont(u8g2_font_courB08_tf);
      font_size = 8;
    break;
    case 10:
      u8g2.setFont(u8g2_font_courB10_tf);
      font_size = 10;
    break;
  }
}

// ------------------------------ ATTACK CIRCLE ---------------------------------

//-------------------------------------------------------------------------------

// ------------------------------- MENU -----------------------------------
void showMissionMenu() {
  u8g2.firstPage();
  do {
    for (it = 0; it < 4; it++)
    {
      if(pointer_counter > 1) {
        u8g2.drawUTF8(4, 4+16*menu_current, ">");
      } else {
        u8g2.drawUTF8(6, 4+16*menu_current, ">");
      }
      u8g2.drawUTF8(16, 4+16*it, strcpy_P(str_buffer, (char*)pgm_read_word(&(main_menu[it]))));
    }
  } while (u8g2.nextPage());
  pointer_counter = pointer_counter == 2 ? 0 : pointer_counter + 1;
}

void showActionMenu() {
  if(var_aux == 255) {
    //Villager
    if(turn_order.at(it) == 0) {
      oled_string = "Aldeão";
    }

    var_aux = 0;
  }

  u8g2.firstPage();
  do {
    for (aux_it = 0; aux_it < 2; aux_it++)
    {
      setFontsize(10);
      u8g2.drawUTF8(28, 4, oled_string.c_str());
      setFontsize(8);
      if(pointer_counter > 1) {
        u8g2.drawUTF8(4, 4+16*(menu_current+2), ">");
      } else {
        u8g2.drawUTF8(6, 4+16*(menu_current+2), ">");
      }
      u8g2.drawUTF8(16, 4+16*(aux_it+2), strcpy_P(str_buffer, (char*)pgm_read_word(&(action_menu[aux_it]))));
    }
  } while (u8g2.nextPage());
  pointer_counter = pointer_counter == 2 ? 0 : pointer_counter + 1;
}

void showWeaponMenu() {
  if(var_aux == 255) {
    //Villager
    if(turn_order.at(it) == 0) {
      oled_string = "Aldeão";
    }

    var_aux = turn_order.at(it);
  }

  u8g2.firstPage();
  do {
    for (aux_it = 0; aux_it < 2; aux_it++)
    {
      setFontsize(10);
      u8g2.drawUTF8(28, 4, oled_string.c_str());
      setFontsize(8);
      if(pointer_counter > 1) {
        u8g2.drawUTF8(4, 4+16*(menu_current+1), ">");
      } else {
        u8g2.drawUTF8(6, 4+16*(menu_current+1), ">");
      }
      switch(var_aux) {
        //Villager
        case 0:
          u8g2.drawUTF8(16, 4+16*(aux_it+1), strcpy_P(str_buffer, (char*)pgm_read_word(&(villager_weapons[aux_it]))));
        break;
      }
    }
    u8g2.drawUTF8(16, 4+16*(aux_it+1), strcpy_P(str_buffer, (char*)pgm_read_word(&(action_menu[2]))));
  } while (u8g2.nextPage());
  pointer_counter = pointer_counter == 2 ? 0 : pointer_counter + 1;
}

//-------------------------------------------------------------------------
void openedInterruption() {
  if(isClosed()) {
    previous_state = current_state;
    current_state = tansition(current_state, 2);
  }
}

void attackCircleSetup() {
  for(it=0; it<attack_circle_pins_amt; it++) {
    pinMode(pgm_read_word_near(attack_circle_pins + it), OUTPUT);
  }
}

//-------------------------------------------------------------------------

void setup() {
  Serial.begin(9600);
  while (!Serial);
  buttonsSetup();
  attackCircleSetup();
  setFontsize(10);

  pinMode(2, OUTPUT);
  digitalWrite(2, HIGH);
    
  u8g2.setFontRefHeightExtendedText();
  u8g2.setDrawColor(1);
  u8g2.setFontPosTop();
  u8g2.setFontDirection(0);
  u8g2.begin();
  
  oled_string.reserve(65);

  previous_state = current_state;
  current_state = tansition(current_state, 1);

  bt_sl_clicked = false;
  bt_lt_clicked = false;
  bt_rt_clicked = false;

  pointer_counter = 0;
  delay(200);
}

void loop() {
  // 0: god, 1: begin, 2: opened, 3: closed, 4: reading_tag, 5: mission_menu
  //6: battle_prologue, 7: battle, 8: battle_epilogue, 9: mission_end, 253: SD READ TEST,254: initialization_check
  switch(current_state) {
    //GOD
    case 0:
      if(Serial.available() > 0) {
        previous_state = current_state;
        current_state = tansition(current_state, Serial.parseInt());
        delay(200);
      }
    break;
    //BEGIN
    case 1:
      if(isClosed()) {        
        previous_state = current_state;
        current_state = tansition(current_state, 3);
      } else {
        previous_state = current_state;
        current_state = tansition(current_state, 2);
      }
      showStateInfo();
      delay(10);
    break;
    //OPENED
    case 2:
      //TODO: Suspend and save state.
      //TODO: Show logo blinking some time
      if(isClosed()) {
        aux_state = previous_state;
        previous_state = current_state;
        current_state = tansition(current_state, aux_state);
        showStateInfo();
      }
    break;
    //CLOSED
    case 3:
      //TODO: Recover state and goTO
      clearOledString();
      setFontsize(10);
      oled_string = "Bem vindo à Aventura estimador leitor!";
      printStrInOled(4, 4);
      delay(1000);
      menu_current = 0;
      previous_state = current_state;
      current_state = tansition(current_state, 5);
      showStateInfo();
      delay(50);
      
//      openedInterruption();
    break;
    case 5:
      setFontsize(8);
      showMissionMenu();
      
      bt_sl_clicked = buttonClicked(7, bt_sl_clicked);
      bt_rt_clicked = buttonClicked(6, bt_sl_clicked);
      bt_lt_clicked = buttonClicked(5, bt_sl_clicked);
      
      if(bt_sl_clicked) {
        if(menu_current == 0) {
          nav_begin = true;
          bt_sl_clicked = false;
          bt_rt_clicked = false;
          bt_lt_clicked = false;
          
          previous_state = current_state;
          current_state = tansition(current_state, 6);
          showStateInfo();
          delay(200);
        } else {
          //TODO: Bip Wrong
        }
      } else if(bt_rt_clicked) {
        //move up
        if(menu_current > 0) {
          menu_current = menu_current - 1;
          delay(20);
        } else {
          //TODO: Bip Wrong
        }
      } else if(bt_lt_clicked) {
        if(menu_current < 3) {
          menu_current = menu_current + 1;
          delay(20);
        } else {
          //TODO: Bip Wrong
        }
      }
      
//      openedInterruption();
      delay(20);
    break;
    //PROLOGUE
    case 6:
      showPrologue();

      bt_sl_clicked = buttonClicked(7, bt_sl_clicked);
      bt_rt_clicked = buttonClicked(6, bt_sl_clicked);
      bt_lt_clicked = buttonClicked(5, bt_sl_clicked);
      
      if(bt_sl_clicked) {
        previous_state = current_state;
        current_state = tansition(current_state, 7);
        showStateInfo();
        bt_sl_clicked = false;
        bt_rt_clicked = false;
        bt_lt_clicked = false;
        nav_begin = true;
        battling = false;
        delay(200);
      } else if(bt_rt_clicked) {
        if(pages_counter < 3) {
          pages_counter = pages_counter + 1;
          delay(20);
        } else {
          //TODO: Bip Wrong
        }
      } else if(bt_lt_clicked) {
        if(pages_counter > 0) {
          pages_counter = pages_counter - 1;
          delay(20);
        } else {
          //TODO: Bip Wrong
        }
      }
//      openedInterruption();
      delay(20);
    break;
    //BATTLE
    case 7:
      if(not battling) {
        turn = 1;
        players = {0};
        villager_x = 0;
        villager_y = 0;
        
        enemies = {"Bandido 1", "Bandido 2"};

        enemies_base_hearts = {2, 3};
        enemies_hearts = {2, 3};
        enemies_base_dmg = {1, 0};
        //10 = 1m
        enemies_range = {12, 15};

        //x: 0 - 150
        //y: 0 - 150
        enemies_x = {150, 130};
        enemies_y = {120, 140};
        
        turn_order.clear();
        turn_order.insert(0, players[0]);
        
        turn_it = 0;

        clearOledString();
        setFontsize(10);
        oled_string = "À Batalha!";
        printStrInOled(8, 16);
        delay(1000);
        clearOledString();
        setFontsize(10);
        oled_string = "Turno " + String(turn);
        printStrInOled(28, 16);
        delay(1000);
        pointer_counter = 0;
        it = 0;
        var_aux = 255;
        clearOledString();
        battling = true;
        selecting_action = true;
        selecting_weapon = false;
        analizing_target = false;
        attack_circle = false;
      } else {
        if(selecting_action) {
          showActionMenu();
  
          bt_sl_clicked = buttonClicked(7, bt_sl_clicked);
          bt_rt_clicked = buttonClicked(6, bt_sl_clicked);
          bt_lt_clicked = buttonClicked(5, bt_sl_clicked);
    
          if(bt_sl_clicked) {
            bt_sl_clicked = false;
            bt_rt_clicked = false;
            bt_lt_clicked = false;
            pointer_counter = 0;
            it = 0;
            var_aux = 255;
            clearOledString();
            selecting_action = false;
            selecting_weapon = menu_current == 0;
            analizing_target = menu_current == 1;
            delay(200);
          } else if(bt_rt_clicked) {
            //move up
            if(menu_current > 0) {
              menu_current = menu_current - 1;
              delay(20);
            } else {
              //TODO: Bip Wrong
            }
          } else if(bt_lt_clicked) {
            if(menu_current < 1) {
              menu_current = menu_current + 1;
              delay(20);
            } else {
              //TODO: Bip Wrong
            }
          }
        } else if(selecting_weapon) {
          showWeaponMenu();
  
          bt_sl_clicked = buttonClicked(7, bt_sl_clicked);
          bt_rt_clicked = buttonClicked(6, bt_sl_clicked);
          bt_lt_clicked = buttonClicked(5, bt_sl_clicked);
    
          if(bt_sl_clicked) {
            bt_sl_clicked = false;
            bt_rt_clicked = false;
            bt_lt_clicked = false;
            getMinimunDistance();

            if(menu_current == 1 and distance_to_enemy < bow_1_short_range) {
              clearOledString();
              setFontsize(8);
              oled_string = "O inimigo está muito próximo para atacá-lo com o arco!";
              printStrInOled(4, 4);
              delay(3000);
              clearOledString();
            } else {             
              //menu current == selected weapon
              selecting_weapon = false;
              attack_circle = menu_current < 2;
              selecting_action = menu_current == 2;
              aux_sequence.clear();
            }
            pointer_counter = 0;
            it = 0;
            var_aux = 255;
            clearOledString();
            delay(200);
          } else if(bt_rt_clicked) {
            //move up
            if(menu_current > 0) {
              menu_current = menu_current - 1;
              delay(20);
            } else {
              //TODO: Bip Wrong
            }
          } else if(bt_lt_clicked) {
            if(menu_current < 2) {
              menu_current = menu_current + 1;
              delay(20);
            } else {
              //TODO: Bip Wrong
            }
          }
        } else if(attack_circle) {
          if(aux_sequence.size() == 0) {
            switch(turn_order.at(0)) {
              //Villager
              case 0:
                aux_sequence = menu_current == 0 ? knife_1_sequence : bow_1_sequence;
                aux_delay = menu_current == 0 ? knife_1_delay : bow_1_delay;
              break;
            }              
            circle_it = 0;
            digitalWrite(2, LOW);
          }    
          clearOledString();
          setFontsize(8);
          oled_string = strcpy_P(str_buffer, (char*)pgm_read_word(&(villager_weapons_ready[menu_current])));
          printStrInOled(4, 4);      
          Serial.println(aux_sequence.at(circle_it));
          for(it=0; it<attack_circle_pins_amt; it++) {
            aux_it = pgm_read_word_near(attack_circle_pins + it);
            digitalWrite(aux_it, bitRead(aux_sequence.at(circle_it), it));
          }

          bt_sl_clicked = buttonClicked(7, bt_sl_clicked);
    
          if(bt_sl_clicked) {
            bt_sl_clicked = false;

            var_aux=0;

            do {
              Serial.println("Blink");
              digitalWrite(2, HIGH);
              delay(200);
              digitalWrite(2, LOW);
              for(it=0; it<attack_circle_pins_amt; it++) {
                aux_it = pgm_read_word_near(attack_circle_pins + it);
                digitalWrite(aux_it, bitRead(aux_sequence.at(circle_it), it));
              }
              delay(200);
              var_aux = var_aux + 1;
            } while(var_aux < 3);
                    
            attack_circle = false;
            analizing_target = false;
            not_attack = false;
            Serial.println("Attack End");
          } else {     
            delay(aux_delay);
            circle_it = circle_it + 1 == aux_sequence.size() ? 0 : circle_it + 1;
            Serial.println("Attack Else");
          }
        } else if(analizing_target){
          getMinimunDistance();
            
          clearOledString();
          setFontsize(8);
          oled_string = "Aldeão: O " + enemies.at(closest_enemy);
          oled_string = oled_string + " está a aproximadamente ";
          oled_string = oled_string + (distance_to_enemy + random(0, 4));
          oled_string = oled_string + " dm de distância.";
          printStrInOled(4, 4);
          delay(3000);
          clearOledString();

          attack_circle = false;
          analizing_target = false;
          not_attack = true;
          delay(20);
        } else {
          getMinimunDistance();

          if(not not_attack) {
            clearOledString();
            setFontsize(8);
            oled_string = strcpy_P(str_buffer, (char*)pgm_read_word(&(villager_attack_pharses[random(0, 3)])));
            
            if(menu_current == 0) {
              if(distance_to_enemy < knife_1_range) {
                oled_string = oled_string + strcpy_P(str_buffer, (char*)pgm_read_word(&(villager_weapon_1_damage[strenghts.at(aux_sequence.at(circle_it))])));
                enemies_hearts.at(closest_enemy) = knife_1_base_dmg + strenghts.at(aux_sequence.at(circle_it)) > enemies_hearts.at(closest_enemy) ? 0 : enemies_hearts.at(closest_enemy) - knife_1_base_dmg + strenghts.at(aux_sequence.at(circle_it));
              } else {
                clearOledString();
                setFontsize(8);
                oled_string = "Aldeão: O inimigo está além do alcance da faca...";
                printStrInOled(4, 4);
                delay(3000);
                not_attack = true;
              }
            } else {
              if(distance_to_enemy < bow_1_range) {
                oled_string = oled_string + strcpy_P(str_buffer, (char*)pgm_read_word(&(villager_weapon_2_damage[strenghts.at(aux_sequence.at(circle_it))])));
                enemies_hearts.at(closest_enemy) = bow_1_base_dmg + strenghts.at(aux_sequence.at(circle_it)) > enemies_hearts.at(closest_enemy) ? 0 : enemies_hearts.at(closest_enemy) - knife_1_base_dmg + strenghts.at(aux_sequence.at(circle_it));
              } else {
                clearOledString();
                setFontsize(8);
                oled_string = "Aldeão: O inimigo está além do alcance do arco...";
                printStrInOled(4, 4);
                delay(3000);
                not_attack = true;
              }
            }

            if(not not_attack) {
              printStrInOled(4, 4); 
              delay(6000);
              
              //Check if everyone was defeated
              if(enemies_hearts.at(closest_enemy) == 0) {
                clearOledString();
                setFontsize(8);
                oled_string = "O " + enemies.at(closest_enemy);
                oled_string = oled_string + " caiu e está desmaiado.";
                printStrInOled(4, 4);
                delay(3000);
              } else {
                clearOledString();
                setFontsize(8);
                oled_string = strcpy_P(str_buffer, (char*)pgm_read_word(&(villager_post_attack_pharses[random(0, 4)])));
                printStrInOled(4, 4);
                delay(3000);
              }
            }
          }         

          //enemy move
          if(enemies_hearts.at(0) != 0) {
            clearOledString();
            setFontsize(8);
            aproach = random(0, 25);
            oled_string = "O " + enemies.at(0);
            oled_string = oled_string + " aproximou-se ";
            oled_string = oled_string + String(aproach);
            oled_string = oled_string + " dm.";
            printStrInOled(4, 4);
            delay(3000);

            enemies_x.at(0) = enemies_x.at(0) - aproach > 5 ? enemies_x.at(0) - aproach : 5;
            enemies_y.at(0) = enemies_y.at(0) - aproach > 5 ? enemies_y.at(0) - aproach : 5;
          }

          if(enemies_hearts.at(1) != 0) {
            clearOledString();
            setFontsize(8);
            aproach = random(0, 20);
            oled_string = "O " + enemies.at(1);
            oled_string = oled_string + " aproximou-se ";
            oled_string = oled_string + String(aproach);
            oled_string = oled_string + " dm.";
            printStrInOled(4, 4);
            delay(3000);

            enemies_x.at(1) = enemies_x.at(1) - aproach > 5 ? enemies_x.at(1) - aproach : 5;
            enemies_y.at(1) = enemies_y.at(1) - aproach > 5 ? enemies_y.at(1) - aproach : 5;
          }

          //just one enemy attack
          getMinimunDistance();

          if(distance_to_enemy < enemies_range.at(closest_enemy)) {
            clearOledString();
            setFontsize(8);
            aproach = random(0, 2);
            oled_string = "O " + enemies.at(closest_enemy);
            oled_string = oled_string + " acertou Aldeão com sua lâmina perversa.";
            printStrInOled(4, 4);
            delay(3000);

            villager_hearts = villager_hearts - aproach > 0 ? villager_hearts - aproach : 0;
          }

          if(enemies_hearts.at(0) == 0 and enemies_hearts.at(1) == 0) {
            clearOledString();
            setFontsize(10);
            oled_string = "Vitória!";
            printStrInOled(24, 16);
            delay(1000);
            previous_state = current_state;
            current_state = tansition(current_state, 8);
            showStateInfo();
            delay(50);

            aux_sequence.clear();
            aux_delay = 0;
            circle_it = 0;
            menu_current = 0;
            pointer_counter = 0;
            clearOledString();
            var_aux = 255;
            selecting_action = false;
            selecting_weapon = false;
            analizing_target = false;
            attack_circle = false;
            not_attack = false;
          } else if(villager_hearts == 0){
            clearOledString();
            setFontsize(10);
            oled_string = "Trágico Fim.";
            printStrInOled(4, 16);
            delay(2000);
            previous_state = current_state;
            current_state = tansition(current_state, 1);
            showStateInfo();
            delay(50);
          } else {
            if(turn_it + 1 == turn_order.size()) {
              Serial.println("Turn end");
              turn_it = 0;
              turn = turn + 1;
              //test only
              turn_order.clear();
              turn_order.insert(0, players[0]);
    
              clearOledString();
              setFontsize(10);
              oled_string = "Turno " + String(turn);
              printStrInOled(28, 16);
              delay(1000);
              it = 0;
            } else {
              turn_it = turn_it + 1;
            }
          }

          aux_sequence.clear();
          aux_delay = 0;
          circle_it = 0;
          menu_current = 0;
          pointer_counter = 0;
          clearOledString();
          var_aux = 255;
          selecting_action = true;
          selecting_weapon = false;
          analizing_target = false;
          attack_circle = false;
          not_attack = false;
        }
      }      
      delay(20);
    break;
    //EPILOGUE
    case 8:
      showEpilogue();

      bt_sl_clicked = buttonClicked(7, bt_sl_clicked);
      bt_rt_clicked = buttonClicked(6, bt_sl_clicked);
      bt_lt_clicked = buttonClicked(5, bt_sl_clicked);

      if(bt_sl_clicked) {
        previous_state = current_state;
        current_state = tansition(current_state, 1);
        showStateInfo();
        bt_sl_clicked = false;
        bt_rt_clicked = false;
        bt_lt_clicked = false;
        nav_begin = true;
        delay(200);
      } else if(bt_rt_clicked) {
        if(pages_counter < 3) {
          pages_counter = pages_counter + 1;
          delay(20);
        } else {
          //TODO: Bip Wrong
        }
      } else if(bt_lt_clicked) {
        if(pages_counter > 0) {
          pages_counter = pages_counter - 1;
          delay(20);
        } else {
          //TODO: Bip Wrong
        }
      }
    break;
  }
}
