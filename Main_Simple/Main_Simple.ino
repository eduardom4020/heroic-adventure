#include <avr/pgmspace.h>
#include <SPI.h>
#include <SD.h>
#include <U8g2lib.h>

//PINOUT:
//  13 -> SD Card SCK
//  12 -> SD Card Miso
//  11 -> SD Card Mosi
//  10 -> Attack Circle MSB
//  9 ->  Attack Circle bit
//  8 ->  Attack Circle bit
//  7 ->  Select Button
//  6 ->  Right Button
//  5 ->  Left Button
//  4 ->  SD Card CS
//  3 ->  Attack Circle LSB
//  2 ->  Free
//  1 ->  TX
//  0 ->  RX

// A5 ->  SCL -> OLED SCL
// A4 ->  SDA -> OLED SDA
// A3 ->  Free
// A2 ->  Free
// A1 ->  Free
// A0 ->  Free

// -----------------------------  Constants -------------------------------------
byte it;
byte aux_it;

byte var_aux;

char char_it;

File file;

String oled_string;

// 0: select - pin7 ; 1: right - pin6 ; 2: left - pin5
const byte buttons_amt = 3;
const PROGMEM byte buttons[] = {7, 6, 5};

byte current_state = 254;
byte previous_state = current_state;
byte aux_state;

byte menu_current;
//byte menu_previous;

const char main_menu_1[] PROGMEM = "Aventureiros";
const char menu_empty[] PROGMEM = "--";

const char* const main_menu[] PROGMEM = {main_menu_1, menu_empty, menu_empty, menu_empty};

char menu_buffer[25];

bool bt_sl_clicked;
bool bt_lt_clicked;
bool bt_rt_clicked;

// ----------------------------- UTILS ------------------------------------------
void clearOledString() {
  oled_string = ""; 
}

void showStateInfo() {
  Serial.print(F("Previous State: ")); Serial.print(previous_state);
  Serial.print(F(" Current State: ")); Serial.println(current_state);
}

// ------------------------------- BUTTON ---------------------------------------
// 0: select - pin7 ; 1: right - pin6 ; 2: left - pin5
void setButton(byte buttonPin) {
  pinMode(buttonPin, INPUT);
}

void buttonsSetup() {
  for(it=0; it<buttons_amt; it++) {
    setButton(pgm_read_word_near(buttons + it));
  }
}

/*Method Usage: 
    bt_x_clicked = false;
    bt_x_clicked = buttonClicked(x, bt_x_pressed)
    if(bt_x_clicked) {...}
*/
bool buttonClicked(byte buttonPin, bool pressed) {
  if (digitalRead(buttonPin) == HIGH) {
    if(!pressed) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

//-------------------------------------------------------------------------------

// ------------------------------- STATES --------------------------------------
// States index

// 0: god, 1: begin, 2: opened, 3: closed, 4: reading_tag, 5: mission_menu
//6: battle, 7: mission_end, 253: SD READ TEST,254: initialization_check

bool canStateChangeTo(byte current, byte state) {
  switch(current) {
    case 0: return true;      
    case 1: return ( state == 3 || state == 2 || state == 0 );
    case 2: return ( state == 1 || state == 0 );
    case 3: return ( state == 5 || state == 2 || state == 0 );
    //case 4: return ( state == 5 || state == 2 || state == 0 );
    case 5: return ( state == 6 || state == 2 || state == 0 );
    case 6: return ( state == 7 || state == 2 || state == 0 );
    case 7: return ( state == 3 || state == 2 || state == 0 );

    case 254: return ( state == 1 || state == 0 );
  }
}

/*Method Usage: 
    result = tansition(current_state, next_state);
    if(result < 255 and result != current_state) {
      previous_state = current_state;
      current_state = tansition(current_state, next_state);
    }
    switch(current_state) {...}
*/
byte tansition(byte current, byte state) {
  if(canStateChangeTo(current, state)) {
    return state;
  } else {
    return 255;
  }
}

//-------------------------------------------------------------------------------

// ------------------------------- SD MANAGER -----------------------------------
void sdSetup() {
  if (SD.begin(4)) {
    Serial.println(F("sd card initialization success!"));
  } else {
    Serial.println(F("sd card initialization failed!"));
  }
}

void readTableCell(String file_name, byte column, byte row) {
  Serial.println(F("Reading Table Function"));
  Serial.println(file_name);
  file = SD.open(file_name);

  if(column < 1) {
    column = 1;
  }

  if(row < 1) {
    row = 1;
  }
  
  clearOledString();
  
  it = 0;
  aux_it = 0;
  
  while (file.available()) {
      char_it = file.read();
      if(row == aux_it and column == it) {      
        oled_string = oled_string + char_it;

        if (char_it == '|' or char_it == '\n') {
          return;
        }
      } else {
        if (char_it == '|') {
            it = it + 1;
        } else if (char_it == '\n') {
            aux_it = aux_it + 1;
            it = 0;
        }
      }
  }
  return;
}

//-------------------------------------------------------------------------------

// ---------------------------------- LDR ---------------------------------------
bool isClosed() {
  return true;
//  return analogRead(0) < 20;
}

//-------------------------------------------------------------------------------

// --------------------------------- BATTLE -------------------------------------
//bool showPrologue(byte page) {
//  
//}
//         if(!finished_prologue) {
//           battle_progress_counter = battle1.prologue(battle_progress_counter);
//         } else if(!finished_epilogue){
//           battle_progress_counter = battle1.epilogue(battle_progress_counter);
//         }
//
//         if(right_button.clicked()) {
//           battle_progress_counter = battle_progress_counter + 1;
//         } else if (left_button.clicked()) {
//           battle_progress_counter = battle_progress_counter - 1;
//         } else if (select_button.clicked()) {
//           if(!finished_prologue) {
//             finished_prologue = true;
//           } else if(!finished_epilogue){
//             finished_epilogue = true;
//
//             if(!app.isGOD) {
//               app.toMissionMenu();
//             } else {
//               app.toGODMode();
//             }
//           }
//         }

//-------------------------------------------------------------------------------

// --------------------------------- OLED ---------------------------------------
U8G2_SSD1306_128X64_NONAME_1_SW_I2C u8g2(U8G2_R0, /* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);

void drawOledStr(byte x, byte y) {
  aux_it = 0;
  var_aux = y;
  it=0;
  while(it < oled_string.length()) {
    if(it - aux_it == 14 or (it - aux_it > 10 and oled_string.charAt(it) == ' ')) {
      u8g2.drawUTF8(x, y, oled_string.substring(aux_it, it+1).c_str());
      y = y + 12;
      aux_it = it + 1;
      if(oled_string.charAt(aux_it) == ' ') {
        aux_it = aux_it + 1;
        it = it + 1;
      }
    }
    it = it + 1;
  }

  if(aux_it < it) {
    u8g2.drawUTF8(x, y, oled_string.substring(aux_it, it+1).c_str());
  }

  y = var_aux;
}

void printStrInOled(byte x, byte y) {
  u8g2.firstPage();
  do {
    drawOledStr(x, y);
  } while (u8g2.nextPage());
}

// ------------------------------ ATTACK CIRCLE ---------------------------------

//-------------------------------------------------------------------------------

// AttackCircle attack_circle;
// ApplicationState app;

// byte attack_result;
// bool blink_end;

// int run_delay;

// byte battle_progress_counter;
// bool finished_prologue;
// bool finished_epilogue;

//Battle battle1(1, 4, 2);

// ------------------------------- MENU -----------------------------------
void showMissionMenu() {
  u8g2.firstPage();
  do {
    for (int i = 0; i < 4; i++)
    {
      if(millis() < 500) {
        u8g2.drawUTF8(4, 4+16*menu_current, ">");
      } else {
        u8g2.drawUTF8(6, 4+16*menu_current, ">");
      }
      u8g2.drawUTF8(8, 4+16*i, strcpy_P(menu_buffer, (char*)pgm_read_word(&(main_menu[i]))));
    }
  } while (u8g2.nextPage());
}

//-------------------------------------------------------------------------
void openedInterruption() {
  if(isClosed()) {
    previous_state = current_state;
    current_state = tansition(current_state, 2);
  }
}

//-------------------------------------------------------------------------

void setup() {
  Serial.begin(9600);
  while (!Serial);
  buttonsSetup();
  sdSetup();

  u8g2.setFont(u8g2_font_courB10_tf);
  u8g2.setFontRefHeightExtendedText();
  u8g2.setDrawColor(1);
  u8g2.setFontPosTop();
  u8g2.setFontDirection(0);
  u8g2.begin();
  
  oled_string.reserve(65);

  previous_state = current_state;
  current_state = tansition(current_state, 1);

  bt_sl_clicked = false;
  bt_lt_clicked = false;
  bt_rt_clicked = false;
  
//  clearOLED();

//  current_state = pgm_read_word_near(main_states + 8);
//  previous_state = current_state;

  // attack_result = 255;
  // //for tests only
  // app.toGODMode();
  // battle_progress_counter = 0;
  // finished_prologue = false;
  // finished_epilogue = false;
  delay(200);
}

void loop() {
  // 0: god, 1: begin, 2: opened, 3: closed, 4: reading_tag, 5: mission_menu
  //6: battle, 7: mission_end, 253: SD READ TEST,254: initialization_check
  switch(current_state) {
    //GOD
    case 0:
      if(Serial.available() > 0) {
        previous_state = current_state;
        current_state = tansition(current_state, Serial.parseInt());
        Serial.print(F("To State "));
        Serial.println(current_state);
        delay(200);
      }
    break;
    //BEGIN
    case 1:
      if(isClosed()) {        
        previous_state = current_state;
        current_state = tansition(current_state, 3);
      } else {
        Serial.print(F("Oi"));
        previous_state = current_state;
        current_state = tansition(current_state, 2);
      }
      showStateInfo();
      delay(10);
    break;
    //OPENED
    case 2:
      //TODO: Suspend and save state.
      //TODO: Show logo blinking some time
      if(isClosed()) {
        aux_state = previous_state;
        previous_state = current_state;
        current_state = tansition(current_state, aux_state);
        showStateInfo();
      }
    break;
    //CLOSED
    case 3:
      //TODO: Recover state and goTO
      clearOledString();
      oled_string = previous_state == 2 ? "O Marcador de Página foi retirado! Bem vindo de volta!" : "Bem vindo à Aventura nobre ser superior!";
      printStrInOled(4, 4);
      delay(1000);
      menu_current = 0;
      previous_state = current_state;
      current_state = tansition(current_state, 5);
      showStateInfo();
      delay(50);
      
//      openedInterruption();
    break;
    case 5:
      showMissionMenu();
      Serial.println(F("At Mission Menu"));
      
      bt_sl_clicked = buttonClicked(7, bt_sl_clicked);
      bt_rt_clicked = buttonClicked(6, bt_sl_clicked);
      bt_lt_clicked = buttonClicked(5, bt_sl_clicked);
      
      if(bt_sl_clicked) {
        Serial.println(F("Click!"));
        
        if(menu_current == 0) {
          previous_state = current_state;
          current_state = tansition(current_state, 6);
          showStateInfo();
          delay(50);

          clearOledString();
          oled_string = "Prólogo";
          printStrInOled(32, 16);
          delay(2000);
        } else {
          //TODO: Bip Wrong
        }
      } else if(bt_rt_clicked) {
        //move up
        if(menu_current > 0) {
          menu_current = menu_current - 1;
        } else {
          //TODO: Bip Wrong
        }
      } else if(bt_lt_clicked) {
        if(menu_current < 4) {
          menu_current = menu_current + 1;
        } else {
          //TODO: Bip Wrong
        }
      }
      
//      openedInterruption();
      delay(20);
    break;
    case 6:
      Serial.println(F("At Mission"));
      clearOledString();
      readTableCell("bt1pro.txt", 1, 1);
      printStrInOled(4, 4);
      delay(1000);
      
//      openedInterruption();
      delay(10);
    break;
    //SD READ TEST
//    case 253:
//      readTableCell("bt1pro.txt", 1, 1);
//      printStrInOled(4, 4);
//      delay(1000);
//
//      aux_state = previous_state;
//      previous_state = current_state;
//      current_state = tansition(current_state, aux_state);
//      delay(200);
//
//    break;
    //HEALTH CHECK
    case 254:
      //TODO: Implement checks
      previous_state = current_state;
      current_state = tansition(current_state, 1);
      showStateInfo();
      delay(200);
    break;

    //BEGIN
    // case 1:
    //   if(isBookClosed()) {
    //     app.toClosed();
    //   } else {
    //     app.toOpened();
    //   }
    // break;
//    case OPENED:
//      suspend();
//      if(isBookClosed()) {
//        app.toClosed();
//      }
//    break;
//    case CLOSED:
//      if(app.getPrevState() == BEGIN) {
//        app.toReadingTag();
//      }   
//      //TODO: Implement recovering  
//    break;
//    case READING_TAG:
//      current_mission = readTag();
//      app.toMissionMenu();
//    break;

//       case ATTACK_CIRCLE:
//         if(!attack_circle.isRunning()) {
//           attack_circle.start();
//         }
//         attack_circle.setLEDsOff();
//         delay(50);
//         run_delay = attack_circle.run(0);
//         delay(run_delay);
        
//         if(select_button.clicked()) {
// //          Serial.println("Clicked");
//           attack_result = attack_circle.stop();
//         }
      
//         if(attack_result < 255) {
// //          Serial.println("Blink");
//           if(attack_circle.blinkLED(attack_result)) {
//             delay(attack_circle.blinkDelay());
//           } else { 
//             delay(500);
//             attack_circle.setLEDsOff();

//             //temporary
//             if(!app.isGOD) {
//               app.toPerformAction();
//             } else {
//               app.toGODMode();
//             }
//           }
//         }
//       break;

//       case BATTLE:


//         delay(50);
//       break;
  }

//  delay(10);
}
